@extends('admin.layouts.master')
@section('page-title', 'Edit ' . $page_title . ' - ' . $locale)
@section('breadcrumb')
<li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
{{Form::open(['route'=>['admin.pages.edit', $locale, $page_slug],'method'=>'POST'])}}

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>@yield('page-title')</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                @include('admin.pratical.message')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Content </label>
                            {!! Form::textarea('content', $page_content, ['id' => 'ckeditor']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block"> Save </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{Form::close()}}
@endsection

@section('styles')

@endsection


@section('scripts')

<script src="{{ url('assets/admin/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {
        CKEDITOR.replace('ckeditor');
    });

</script>
@endsection
