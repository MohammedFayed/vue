@include('admin.pratical.header')
@include('admin.pratical.navbar')
@include('admin.pratical.menu')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>@yield('page-title')</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin')}}"><i class="icon-home"></i></a></li>
                        @yield('breadcrumb')
                    </ul>
                </div>
            </div>
        </div>

        @yield('content')

    </div>
</div>

@include('admin.pratical.footer')
