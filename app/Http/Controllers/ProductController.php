<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json(Product::all(),200);
    }
    

    public function show(Product $product)
    {
        return response()->json($product,200); 
    }


}
