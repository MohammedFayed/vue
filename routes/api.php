<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::get('/products_all', 'ProductController@index');
Route::get('/products_single/{product}', 'ProductController@show');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('users/{user}/orders','UserController@showOrders');
//    Route::patch('products/{product}/units/add','ProductController@updateUnits');
//    Route::patch('orders/{order}/deliver','OrderController@deliverOrder');
//    Route::resource('/orders', 'OrderController');
    Route::get('/orders-all', 'OrderController@index');
    Route::post('orders-store', 'OrderController@store');
//    Route::resource('/products', 'ProductController')->except(['index','show']);
});
