<?php

namespace Modules\Languages\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Languages\Models\Local;

class LocalDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Local::create([
            'id' => 1,
            'local_name' => 'English',
            'code' => 'en',
            'direction' => 'ltr',
        ]);

        Local::create([
            'id' => 2,
            'local_name' => 'Arabic',
            'code' => 'ar',
            'direction' => 'rtl',
        ]);

        // $this->call("OthersTableSeeder");
    }
}
