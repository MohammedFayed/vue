<?php

namespace Modules\Languages\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\Languages\Http\Requests\LanguagesRequest;
use Modules\Languages\Models\Local;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $languages = Local::get();
        return view('languages::index',compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('languages::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request|LanguagesRequest $request
     * @return Response
     */
    public function store(LanguagesRequest $request)
    {
        $inputs = $request->all();
        if (Local::create($inputs)) {
            flash()->success('Data was saved successfully');
            return redirect()->route('languages.index');
        } else {
            flash()->error('Failed to save data, please try again later');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('languages::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($local_id)
    {
        $language = Local::findOrFail($local_id);
        return view('languages::edit',compact('language'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request|LanguagesRequest $request
     * @param $local_id
     * @return Response
     */
    public function update(LanguagesRequest $request,$local_id)
    {
        $language = Local::findOrFail($local_id);
        $inputs = $request->all();
        if ($language->update($inputs)) {
            flash()->success('Data was saved successfully');
            return redirect()->route('languages.index');
        } else {
            flash()->error('Failed to save data, please try again later');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($local_id)
    {
        $language = Local::findOrFail($local_id);

        if ($language->delete()) {
            flash()->success('Data was deleted successfully');
        } else {
            flash()->error('failed to delete data, please try again later');
        }
        return redirect()->back();
    }
}
