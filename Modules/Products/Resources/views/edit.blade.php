@extends('admin.layouts.master')
@section('page-title','Edit Product')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/products')}}">Products</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    {{Form::model($product,['route'=>['products.update',$product],'method'=>'PATCH','files'=>true])}}

    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Edit: {{ $product->name }} </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')

                    <div class="form-group">
                        <label>Name</label>
                        {{Form::text('name',$product->name,['class'=>'form-control'])}}
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        {{Form::textarea('description',$product->description,['class'=>'form-control'])}}
                    </div>

                </div>
            </div>



            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Images </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Featured Image</label>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{url('uploads/'.$product->image)}}" alt="" class="img-thumbnail">
                            </div>
                        </div>
                        {{Form::file('image',['class'=>'form-control'])}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Pricing </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Price</label>
                        {{Form::number('price',null,['class'=>'form-control','step'=>'0.01'])}}
                    </div>
                    <div class="form-group">
                        <label>Units</label>
                        {{Form::number('units',null,['class'=>'form-control','step'=>'0.01'])}}
                    </div>

                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Save </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"> Save </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-block"> Cancel </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{Form::close()}}


@endsection




