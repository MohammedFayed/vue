<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ImportToolLogViewer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attributes\Models\Attribute;
use Modules\Branches\Models\Branch;
use Modules\Categories\Models\Category;
use Modules\Classifications\Models\Classification;
use Modules\Identifiers\Models\Identifier;
use Modules\Products\Models\Product;
use Modules\Products\Models\ProductImage;
use Modules\Units\Models\Unit;
use DB;
use File;
use Modules\Attributes\Models\AttributeProduct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class ProductsController extends Controller
{

    protected $list = [];
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('products::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('products::create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'units' => 'required',
            'image' => 'required|image',
        ]);

        $product = new Product();

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->units = $request->units;
        $product->image = $request->image->store('products');
        $product->save();

        return redirect('admin/products')->with(['success' => 'product created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // return view('products::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('products::edit', compact(  'product'));
    }


    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'units' => 'required',
            'image' => 'image',
        ]);


        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->units = $request->units;
        $product->image = ($request->hasFile('image')) ? $request->image->store('products') : $product->image;
        $product->save();

        return redirect('admin/products')->with(['success' => 'product updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('admin/products')->with(['success' => 'product deleted successfully']);

    }


    public function datatable()
    {
        $locations = Product::query();
        return \DataTables::eloquent($locations)
            ->editColumn('image', function ($model) {
                return "<img src='".asset('uploads/'.$model->image)."' width='50px' height='50px'>";
            })
            ->editColumn('options', function ($model) {
                return "<a href='" . url('admin/products/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/products/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";
            })
            ->rawColumns(['options','image'])

            ->make(true);
    }

}
