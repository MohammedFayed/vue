@extends('admin.layouts.master')
@section('page-title','Edit Admin')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href='{{url('admin/user')}}'>Admins</a> </li>

    <li class="breadcrumb-item active">Edit Admin</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <h2>Edit Admin</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::model($admin,['action'=>['\Modules\Admins\Http\Controllers\AdminsController@update',$admin],'method'=>'PATCH'])}}
                @include('admins::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
