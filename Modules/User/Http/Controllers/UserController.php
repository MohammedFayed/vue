<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Models\User;
use \App\Http\Controllers\Controller;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function create()
    {

        return view('user::create');
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
        ]);

        $new_user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return redirect(url('admin/user/'))->with(['success'=>'User Created Successfully']);


    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('user::edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $user = User::findOrFail($id);

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
        ]);

        $new_user = $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return redirect(url('admin/user'))->with(['success'=>'User Updated Successfully']);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $user = User::findOrFail($id);

        if ($user->delete()) {
            return redirect()->back()->with(['success'=>'Data was deleted successfully']);
        } else {
            return redirect()->back()->with(['error'=>'failed to delete data, please try again later']);
        }

    }


    public function datatable(Request $request)
    {
        $users = User::get();
        return \DataTables::of($users)
            ->editColumn('name',function ($model){
                return $model->name;
            })

            ->editColumn('options',function ($model){
                return "<a href='".url('admin/user/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a> 
                        <a href='".url('admin/user/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>  ";
            })
            ->rawColumns(['options'])

            ->make();
    }

}
