<?php

    Route::group(['prefix' => 'admin'], function () {

        Route::get('login', 'AdminAuth@login')->name('admin.login');
        Route::post('login', 'AdminAuth@dologin');
        $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        $this->post('password/reset', 'ResetPasswordController@reset')->name('password.update');

        Route::group(['middleware' => 'admin:admin'], function () {

            Route::get('/dashboard', function () {
                return view('admin.home');
            })->name('home');

            Route::any('logout', 'AdminAuth@logout')->name('doLogout');

        });

    });


