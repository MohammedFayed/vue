<?php

namespace Modules\Orders\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;
use Modules\User\Models\User;

class Order extends Model
{
    protected $fillable = [];


    public function products()
    {
        return $this->belongsTo(Product::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
