<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Orders\Models\Order;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Session;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('orders::index');
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */



    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        // $order->deleteTranslations();
        return redirect('admin/orders')->with(['success' => 'product deleted successfully']);
    }


    public function datatable()
    {
        $orders = Order::with('products')->orderBy('created_at', 'Desc')->get();

        return \DataTables::of($orders)

            ->editColumn('user', function ($model) {
                $user = User::where('id', $model->user_id)->first();

                return $user->name;
            })
            ->editColumn('price', function ($model) {
                $product = Product::where('id', $model->product_id)->first();

                return $product->price * $model->quantity;
            })

            ->addColumn('options', function ($model) {
                return "<a href='" . url('admin/orders/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->rawColumns(['options','status'])
            ->make(true);
    }






}
