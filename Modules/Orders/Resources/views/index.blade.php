@extends('admin.layouts.master')
@section('page-title','Orders')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')

    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Order list</h2>
                </div>

            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Submitted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection



@section('scripts')
    <script>
        $(function () {
            table = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                orderCellsTop: true,
                oLanguage: {
                    sInfo: "Showing_START_to_END_of_TOTAL_items."
                },
                order: [
                    [0, "desc"]
                ],
                ajax: '{!! url("admin/orders/datatable/") !!}',
                columns: [{
                    data: 'id',
                    name: 'id'
                },

                    {
                        data: 'user',
                        name: 'user'
                    },
                    {
                        data: 'price',
                        name: 'price'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity'
                    },
                    {
                        data: 'created_at',
                        name: 'Submitted'
                    },
                    {
                        data: 'options',
                        name: 'options'
                    },
                ],
                dom: 'lr<"table-filter-container">tip',
                initComplete: function (settings) {

                    var api = new $.fn.dataTable.Api(settings);
                    $('.table-filter-container', api.table().container()).append(
                        $('#table-filter').detach().show()
                    );

                    $('#table-filter select').on('change', function () {
                        table.search(this.value).draw();
                    });
                }
            });

            $('#table-filter select').on('change', function () {
                table.search(this.value).draw();
            });


        });

    </script>
@stop
