# Building an E-commerce platform with Laravel and Vue

### Install
Clone the git repository on your computer


```
$ composer install
```


### Setup
- When you are done with installation, copy the `.env.example` file to `.env`

  ```$ cp .env.example to .env```


- Generate the application key

  ```$ php artisan key:generate```

- Migrate the application

  ```$ php artisan migrate```

- Install laravel passport

  ```$ php artisan passport:install```

- Seed Database

  ```$ php artisan db:seed```


- Install node modules

  ```$ npm install```


### Run the application

  ```$ php artisan serve```
  
  
  ### Run to admin
  
    ```$ http://127.0.0.1:8000/admin/login```
    ```password : `123456``
    ```email : admin@admin.com```


