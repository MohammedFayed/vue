<?php

use Illuminate\Database\Seeder;
use App\Product;
use Illuminate\Http\UploadedFile;

class ProductTableSeeder extends Seeder
{

    public function run()
    {

        for ($i = 1; $i <= 5; $i++) {
            $product = Product::create([
                'name' => 'Product' . $i ,
                'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua consequat.",
                'price' => 1000,
                'units' => 2,
                'image' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);



        }


    }

}
